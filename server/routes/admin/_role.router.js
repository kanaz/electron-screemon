import Role from '../../model/role.model';
export default (app, router, auth, isPermitted) => {

  router.route('/role')
    .post((req, res) => {
      // console.log(req.body);
      // res.json(req.body);
      Role.create({
        name: req.body.name,
        description: req.body.description,
        is_enabled: req.body.is_enabled,
        menu: req.body.menu,
        full_generation: req.body.full_generation,
        created_at: new Date(),
        updated_at: new Date(),
        created_by: req.user._id,
        updated_by: req.user._id
      }, (err, role) => {
        if (err)
          res.send(err);
        res.json(role);
      });
    })
    .get((req, res) => {
      Role.find((err, role) => {
        if (err)
          res.send(err);
        else
          res.json(role);
      });
    })

  router.route('/role/:role_id')

    .get((req, res) => {
      Role.findOne({ '_id': req.params.role_id }, (err, role) => {
        if (!err)
          res.json(role);
        else
          res.send(err);
      })
    })

    .put(auth, (req, res) => {
      // return res.send(req.body);
      Role.findOne({

        '_id': req.params.role_id

      }, (err, role) => {
        if (err)
          res.send(err);
        if (req.body._id) {
          role.name = req.body.name;
          role.menu = req.body.menu;
          role.full_generation = req.body.full_generation;
          role.description = req.body.description;
          role.is_enabled = req.body.is_enabled;
          role.updated_by = req.user._id;
          role.updated_at = new Date();
          // role.resources = req.body.resources
        }
        return role.save((err) => {
          if (err)
            res.send(err);
          return res.send(role);
        });
      })
    })

    .delete((req, res) => {
      Role.remove({ _id: req.params.role_id }, (err) => {
        if (err)
          res.send(err);
        else
          res.send({ "message": "Data Deleted", "status": 1 });
      });
    })

}