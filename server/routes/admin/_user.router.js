import User from '../../model/user.model';
var jwt = require('jsonwebtoken');
import config from '../../../config/config.json';

export default (app, router, auth, isPermitted) => {
    router.route('/user')
        .get((req, res, next) => {
            User.find()
                .populate({ path: 'role' })
                .exec((err, users) => {
                    if (err) {
                        res.send(err);
                    }
                    else {
                        res.json(users);
                    }
                });
        });


    router.route('/user/member/listing')
        .get((req, res) => {
            let token = req.cookies.admin_token;
            jwt.verify(token, config.SESSION_SECRET, function (err, decoded) {
                if (err) {
                    res.json({ success: false, message: 'Please login first' });
                } else {
                    let user = decoded;
                    if (user.user_type == "Employee") {
                        let usr = {
                            user_type: "Employee",
                            search_params: ['all', 'dealer', 'agent', 'subagent'],
                            dealers: [],
                            agents: [],
                            subagents: []
                        }

                        User.find()
                            .where('user_type').equals('Distributor')
                            .exec()
                            .then(dealer => {
                                usr.dealers = dealer;
                                return User.find().where('user_type').equals('Agent')
                            })
                            .then(agent => {
                                usr.agents = agent;
                                return User.find().where('user_type').equals('Subagent')
                            })
                            .then(subagent => {
                                usr.subagents = subagent;
                                res.json(usr);
                            })
                    }
                }
            });
        });

    router.route('/distributor')
        .get((req, res, next) => {
            let token = req.cookies.admin_token;
            jwt.verify(token, config.SESSION_SECRET, function (err, decoded) {
                if (err) {
                    res.json({ success: false, message: 'Please login first' });
                } else {
                    let user = decoded;
                    if (user.user_type == "Distributor" || user.user_type == "Agent" || user.user_type == "Subagent") {
                        User.find({ '_id': user._id })
                            .populate({ path: "area", select: 'name district' })
                            .select({ local: 1, area: 1, first_name: 1, last_name: 1 })
                            .exec()
                            .then(user => {
                                res.json(user);
                            })
                    } else if (user.user_type == "Employee") {
                        User.find({ 'user_type': "Distributor" })
                            .populate({ path: "area", select: 'name district' })
                            .select({ local: 1, area: 1, first_name: 1, last_name: 1 })
                            .exec()
                            .then(user => {
                                res.json(user);
                            })
                    } else {
                        res.json("Who are you?");
                    }
                }
            })
        });


    router.route('/agent')
        .get((req, res, next) => {
            let token = req.cookies.admin_token;
            jwt.verify(token, config.SESSION_SECRET, function (err, decoded) {
                if (err) {
                    res.json({ success: false, message: 'Please login first' });
                } else {
                    let user = decoded;
                    if (user.user_type == "Distributor" || user.user_type == "Agent" || user.user_type == "SubAgent") {
                        User.find({ '_id': user._id })
                            .populate({
                                path: "distributor", select: "first_name last_name area",
                                populate: {
                                    path: "area",
                                    select: "name district"
                                }
                            })
                            .select({ local: 1, distributor: 1, first_name: 1, last_name: 1 })
                            .exec()
                            .then(user => {
                                res.json(user);
                            })
                    } else if (user.user_type == "Employee") {
                        User.find({ 'user_type': "Agent" })
                            .populate({
                                path: "distributor", select: "first_name last_name area",
                                populate: {
                                    path: "area",
                                    select: "name district"
                                }
                            })
                            .select({ local: 1, distributor: 1, first_name: 1, last_name: 1 })
                            .exec()
                            .then(user => {
                                res.json(user);
                            })
                    } else {
                        res.json("Who are you?");
                    }
                }
            })
        });

    router.route('/typed-user/:type')
        .get((req, res) => {
            User.find({ "user_type": req.params.type })
                .exec((err, user) => {
                    if (err)
                        res.send(err);
                    res.json(user);
                })
        })

    router.route('/user/:user_id')
        .get((req, res) => {
            User.findOne({ "_id": req.params.user_id })
                .populate({ path: 'area' })
                .exec((err, user) => {
                    if (err)
                        res.send(err);
                    res.json(user);
                })
        })

        .put((req, res) => {
            console.log(req.params.user_id);
            User.findOne({

                '_id': req.params.user_id

            }, (err, user) => {
                if (err)
                    res.send(err);
                if (req.body._id) {
                    user.first_name = req.body.first_name,
                        user.last_name = req.body.last_name,
                        user.area = req.body.area,
                        user.address = req.body.address,
                        user.mobile_number = req.body.mobile_number,
                        user.role = req.body.role,
                        user.is_enabled = req.body.is_enabled
                } else {
                    user.first_name = req.body.first_name;
                    user.last_name = req.body.last_name;
                    user.distributor = req.body.distributor;
                    user.address = req.body.address;
                    user.mobile_number = req.body.mobile_number;
                    user.store = req.body.store;
                    user.local.email = req.body.email;
                    user.is_enabled = req.body.is_enabled;
                }

                return user.save((err) => {

                    if (err)
                        res.send(err);

                    return res.send(user);

                });
            })
        })

        .delete((req, res) => {
            User.remove({ _id: req.params.user_id }, (err) => {
                if (err)
                    res.send(err);
                else
                    res.send({ "message": "User Deleted", "status": 1 });
            });
        });

    router.route('/user-updated')
        .put((req, res) => {
            User.findOne({ '_id': req.params.id })
                .exec((err, user) => {
                    if (user._id) {
                        user.save(err => {
                            res.json({ success: true, result: 'result' });
                        })
                    }
                })
        });

    router.route('/user/search/v1')
        .get((req, res) => {
            var pageNum = 1, itemsPerPage = 10;

            var terms = req.query.search;
            if (req.query.page) {
                pageNum = req.query.page;
            }
            if (req.query.limit) {
                itemsPerPage = req.query.limit;
            }

            let expression = '.*' + terms + '.*';
            let searchConditions = { "name": { $regex: expression, $options: 'i' } };

            User.find(searchConditions)
                .count((err, count) => {
                    return count
                })
                .then(count => {
                    User.find(searchConditions).select({ 'name': 1, 'description': 1, 'role': 1 })
                        .populate({ path: 'role', select: 'name description' })
                        .skip(itemsPerPage * (pageNum - 1))
                        .limit(itemsPerPage)
                        .exec((err, users) => {
                            if (err)
                                res.send(err);
                            else
                                res.json({
                                    'items': users,
                                    'count': count
                                });
                        })
                })
                .catch(err => {
                    res.send(err);
                })
        });
}