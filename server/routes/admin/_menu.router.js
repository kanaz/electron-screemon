import Menu from '../../model/menu.model';
import Role from '../../model/role.model';
var jwt = require('jsonwebtoken');
import config from '../../../config/config.json';
export default (app, router, auth, isPermitted) => {

    router.route('/menu')
        .get((req, res) => {
            var root = {};
            Menu.findOne({ parent: { $exists: false } })
                // .select({ name: 1, left: 1, right: 1 })
                .exec()
                .then(parentMenu => {
                    root = parentMenu;
                    return Menu.find({ parent: parentMenu._id })
                })
                .then(childs => {
                    if (childs.length > 0) {
                        var menues = [];
                        var newRoot = {
                            _id: root._id,
                            name: root.name,
                            left: root.left,
                            right: root.right,
                            hasChildren: true
                        }
                        menues.push(newRoot);
                        res.json(menues);
                    } else {
                        var menues = [];
                        var newRoot = {
                            _id: root._id,
                            name: root.name,
                            left: root.left,
                            right: root.right,
                            hasChildren: false
                        }
                        menues.push(newRoot);
                        res.json(menues);
                    }
                })
        })

        .post(auth, (req, res) => {
            // console.log(req.body);
            if (req.body.parent._id) {
                var currentLeft = req.body.parent.left;
                var childRight;
                let newMenu = new Menu();
                Menu.find({ parent: req.body.parent._id })
                    .sort({ right: -1 })
                    .exec()
                    .then(childs => {
                        if (childs.length > 0) {
                            childRight = childs[0].right;
                            currentLeft = childs[0].left;
                            newMenu.left = childRight + 1;
                            newMenu.right = childRight + 2;
                            return Menu.find({ left: { $gt: currentLeft } })
                        } else {
                            newMenu.left = currentLeft + 1;
                            newMenu.right = currentLeft + 2;
                            return Menu.find({ left: { $gt: currentLeft } })
                        }
                    })
                    .then(leftMenues => {
                        updateLeftCollections(leftMenues)
                            .then(leftUpdated => {
                                return Menu.find({ right: { $gt: currentLeft } })
                            })
                            .then(rightMenues => {
                                updateRightCollections(rightMenues).then(rightUpdated => {
                                    newMenu.path = req.body.path;
                                    newMenu.parent = req.body.parent._id;
                                    newMenu.name = req.body.data.menu.title;
                                    newMenu.permissions = req.body.permissions;
                                    newMenu.is_enabled = req.body.is_enabled;
                                    newMenu.data = req.body.data;
                                    newMenu.created_at = new Date();
                                    newMenu.updated_at = new Date();
                                    newMenu.created_by = req.user._id;
                                    newMenu.updated_by = req.user._id
                                    newMenu.save(err => {
                                        if (!err) {
                                            res.json(newMenu);
                                        }
                                    })
                                });
                            })
                    })
            } else {
                res.json()
            }
        });

    router.route('/menu/detail/:id')
        .get((req, res) => {
            Menu.findOne({ '_id': req.params.id })
                .populate({ path: 'parent' })
                .exec((err, menue) => {
                    if (err)
                        res.send(err);
                    else {
                        res.json(menue);
                    }
                })
        })


    router.route('/menu/:id')
        .get((req, res) => {
            Menu.find({ 'parent': req.params.id })
                .select({ name: 1, left: 1, right: 1 })
                .exec((err, menues) => {
                    if (err)
                        res.send(err);
                    else {
                        checkHasChildren(menues).then(newParents => {
                            res.json(newParents);
                        })
                    }
                })
        })

        .put(auth, (req, res) => {
            Menu.findOne({
                '_id': req.params.id
            }, (err, newMenu) => {
                if (err)
                    res.send(err);
                if (req.body._id) {
                    newMenu.path = req.body.path;;
                    newMenu.name = req.body.data.menu.title;
                    newMenu.permissions = req.body.permissions;
                    newMenu.is_enabled = req.body.is_enabled;
                    newMenu.data = req.body.data;
                    newMenu.created_at = new Date();
                    newMenu.updated_at = new Date();
                    newMenu.created_by = req.user._id;
                    newMenu.updated_by = req.user._id
                }
                return newMenu.save((err) => {
                    if (err)
                        res.send(err);

                    return res.send(newMenu);
                });
            })
        })
        .delete(auth, (req, res) => {
            Menu.remove({ _id: req.params.id }, (err, restData) => {
                if (err)
                    res.send(err);
                else
                    res.send({ "message": "Data Deleted", "status": 1 });
            });
        });

    router.route('/menu/root/childs')
        .get((req, res) => {
            let token = req.cookies.admin_token;
            jwt.verify(token, config.SESSION_SECRET, function (err, decoded) {
                if (err) {
                    res.json({ success: false, message: 'Please login first' });
                } else {
                    let user = decoded;
                    console.log(user);
                    if (user.role.name == 'Development') {
                        Menu.find()
                            .exec()
                            .then(menu => {
                                checkHasChildren(menu).then(newParents => {
                                    res.json(newParents);
                                })
                            })

                    } else {
                        Role.findOne({ '_id': user.role._id })
                            .populate({ path: 'full_generation' })
                            .exec()
                            .then(role => {
                                let ids = role.full_generation;
                                checkHasChildren(ids).then(newParents => {
                                    res.json(newParents);
                                })
                            })
                    }
                }
            })

        });

    function updateRightCollections(menuList) {
        var data = menuList.map(menuObj => {
            return new Promise((resolve, reject) => {
                menuObj.right = menuObj.right + 2;
                menuObj.save(err => {
                    if (!err) {
                        resolve(menuObj);
                    }
                })
            })
        })
        return Promise.all(data);
    }

    function updateLeftCollections(menuList) {
        var data = menuList.map(menuObj => {
            return new Promise((resolve, reject) => {
                menuObj.left = menuObj.left + 2;
                menuObj.save(err => {
                    if (!err) {
                        resolve(menuObj);
                    }
                })
            })
        })
        return Promise.all(data);
    }

    function checkHasChildren(list) {
        var newMenus = list.map(listObj => {
            return new Promise((resolve, reject) => {
                Menu.find()
                    .where('parent').equals(listObj._id)
                    .exec()
                    .then((childs, err) => {
                        if (!err) {
                            if (childs && Array.isArray(childs) && childs.length > 0) {
                                var newObj = {
                                    name: listObj.name,
                                    text: listObj.name,
                                    path: listObj.path,
                                    parent: listObj.parent,
                                    data: listObj.data,
                                    permissions: listObj.permissions,
                                    _id: listObj._id,
                                    left: listObj.left,
                                    right: listObj.right,
                                    hasChildren: true
                                }
                                resolve(newObj);
                            } else {
                                var newObj = {
                                    name: listObj.name,
                                    text: listObj.name,
                                    path: listObj.path,
                                    parent: listObj.parent,
                                    data: listObj.data,
                                    permissions: listObj.permissions,
                                    _id: listObj._id,
                                    left: listObj.left,
                                    right: listObj.right,
                                    hasChildren: false
                                }
                                resolve(newObj);
                            }
                        } else {
                            console.log(err);
                            reject(err);
                        }
                    })
            })
        })
        return Promise.all(newMenus);
    }

}