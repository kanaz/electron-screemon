
var mongoose = require('mongoose');

let monitorUserSchema = new mongoose.Schema({
 
    // username: { type: String },
    // password: { type:String}
       local: {
        username: {
            type: String,
            unique: true,
            required: [true, "User name required"]
        },

        password: {
            type: String,
            required: [true, "Password is required"]
        },

        email: {
            type: String,
            unique: true,
            required: [true, "Email is required"]
        }
    },

     projects: [{type: mongoose.Schema.ObjectId, ref: 'Project'}],

    first_name: {
        type: String,
        required: [true, "First name is required"]
    },
    last_name: {
        type: String,
        required: [true, "Last name is required"]
    },
    is_active: { type: Boolean, default: 1 },
});

//export default mongoose.model('Project', projectSchema);

var MonitorUser = mongoose.model('MonitorUser', monitorUserSchema);

// make this available to our users in our Node applications
module.exports = MonitorUser;