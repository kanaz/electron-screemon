var mongoose = require('mongoose');
let screenshotSchema = new mongoose.Schema({
	userId: { type: String },
	username: { type: String },
	url: { type: String },
	imageFullName: { type: String },
	imageName: { type: String },
	thumbUrl: {type: String},
	macAdd: [],
	ipAdd: [],
	timeqe: {type: Date },

})

var Screenshot = mongoose.model('Screenshot', screenshotSchema);

// make this available to our users in our Node applications
module.exports = Screenshot;