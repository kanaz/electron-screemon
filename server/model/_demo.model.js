import mongoose from 'mongoose';
let demoSchema = new mongoose.Schema({
    name: { type: String },
    amount: { type: Number, default: 0 },
    is_active: { type: Boolean, default: 1 },
})

export default mongoose.model('Demo', demoSchema);