import mongoose from 'mongoose';


let menuSchema = new mongoose.Schema({
    path: {
        type: String
    },
    name: {
        type: String
    },
    parent: {
        type: mongoose.Schema.ObjectId, ref: 'Menu',
    },
    left: {
        type: Number
    },
    right: {
        type: Number
    },
    is_enabled: {
        type: Boolean
    },
    permissions: [],
    data: {
        menu: {
            title: String,
            icon: String,
            selected: Boolean,
            expanded: Boolean
        }
    },
    created_at: { type: Date, default: Date.now },
    created_by: {
        type: mongoose.Schema.ObjectId, ref: 'User',
        required: [true, 'Please login first']
    },
    updated_by: {
        type: mongoose.Schema.ObjectId, ref: 'User',
        required: [true, 'Please login first']
    },
    updated_at: { type: Date, default: Date.now }
})

var Menu = mongoose.model('Menu', menuSchema);

// make this available to our users in our Node applications
module.exports = Menu;

///export default mongoose.model('Menu', menuSchema);