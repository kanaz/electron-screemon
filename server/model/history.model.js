var mongoose = require('mongoose');
let historySchema = new mongoose.Schema({
    userName: { type: String },
    status: { type: String },
    projectName:{ type: String },
    // ipAddress: { type: String },
    // macAddress: { type: String },
    macAddress: [],
	ipAddress: [],
    start_date: { type: Date },
    is_active: { type: Boolean, default: 1 },
})

var HistoryUser = mongoose.model('HistoryUser', historySchema);

// make this available to our users in our Node applications
module.exports = HistoryUser;