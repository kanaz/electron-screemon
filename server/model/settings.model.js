var mongoose = require('mongoose');
let settingsSchema = new mongoose.Schema({
    time_interval: { type: Number },
    
})

//export default mongoose.model('Status', statusSchema);

var Settings = mongoose.model('Settings', settingsSchema);

// make this available to our users in our Node applications
module.exports = Settings;