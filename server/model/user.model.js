
import mongoose from 'mongoose';

import bcrypt from 'bcrypt-nodejs';

let userSchema = mongoose.Schema({

  local: {

    username: {
      type: String,
      unique: true,
      required: [true, "User name required"]
    },

    password: {
      type: String,
      required: [true, "Password is required"]
    },

    email: {
      type: String,
      unique: true,
      required: [true, "Email is required"]
    }
  },
  first_name: {
    type: String,
    required: [true, "First name is required"]
  },
  last_name: {
    type: String,
   // required: [true, "Last name is required"]
  },
  area: {
    type: mongoose.Schema.ObjectId, ref: 'Area'
  },
  address: {
    type: String
  },
  mobile_number: {
    type: String
  },
  user_type: {
    type: String
  },
  distributor: {
    type: mongoose.Schema.ObjectId, ref: 'User'
  },

  agent: {
    type: mongoose.Schema.ObjectId, ref: 'User'
  },

  store: {
    type: mongoose.Schema.ObjectId, ref: 'Store'
  },

  // designation: String,

  role: {
    type: mongoose.Schema.ObjectId, ref: 'Role'
  },

  is_enabled: Boolean

});


userSchema.methods.generateHash = function (password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function (password) {
  return bcrypt.compareSync(password, this.local.password);
};

//export default mongoose.model('User', userSchema);
var User = mongoose.model('User', userSchema);

// make this available to our users in our Node applications
module.exports = User;