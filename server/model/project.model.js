//import mongoose from 'mongoose';

var mongoose = require('mongoose');

let projectSchema = new mongoose.Schema({
    name: { type: String },
    is_active: { type: Boolean, default: true },
})

//export default mongoose.model('Project', projectSchema);

var Project = mongoose.model('Project', projectSchema);

// make this available to our users in our Node applications
module.exports = Project;