var mongoose = require('mongoose');
let statusSchema = new mongoose.Schema({
    name: { type: String },
    is_active: { type: Boolean, default: 1 },
})

//export default mongoose.model('Status', statusSchema);

var Status = mongoose.model('Status', statusSchema);

// make this available to our users in our Node applications
module.exports = Status;