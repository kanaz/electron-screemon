var mongoose = require('mongoose');
let timeIntervalSchema = new mongoose.Schema({
    time_interval: { type: Number },
    
})

//export default mongoose.model('Status', statusSchema);

var TimeInterval = mongoose.model('TimeInterval', timeIntervalSchema);

// make this available to our users in our Node applications
module.exports = TimeInterval;