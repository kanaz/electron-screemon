import mongoose from 'mongoose';


let pageMenuSchema = new mongoose.Schema({
    path: {
        type: String,
    },
    children: [{
        path: {
            type: String,
        }
    }]
})

//export default mongoose.model('Pagemenu', pageMenuSchema);

var Pagemenu = mongoose.model('Pagemenu', pageMenuSchema);

// make this available to our users in our Node applications
module.exports = Pagemenu;