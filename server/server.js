var express = require('express')
var multer = require('multer')
var app = express()
var myParser = require("body-parser");
var cookieParser = require('cookie-parser')
var fs = require('fs-extra'), util = require('util');
var path = require('path');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var jwt = require('jsonwebtoken');
var passwordHash = require('password-hash');
var config = require('./config/config.json');
var im = require('imagemagick');
//config.SESSION_SECRET = "355FC4FE9348639B4E4FED1B8E93C";


var PORT = config.PORT.PUBLIC.DEV;
//console.log(config);
//console.log(config.PORT.PUBLIC.DEV, 'dddddddddddddd');
var cors = require('cors');


app.use(cookieParser())

var connect = mongoose.connect('mongodb://localhost:27017/mydb');
//var connect = mongoose.connect(config.MONGO_URI.DEVELOPMENT);

//import Project from './model/project.model';
var Project = require('./model/project.model');
var HistoryUser = require('./model/history.model');
var Status = require('./model/status.model');
var User = require('./model/user.model');
var Screenshot = require('./model/screenshot.model');
var Setting = require('./model/settings.model');
var TimeInterval = require('./model/timeInterval.model');
var MonitorUser = require('./model/monitor-user.model');
// import History from './model/history.model';
// import Status from './model/status.model';
// import User from './model/user.model';

// var projectSchemaName = new Schema({
// 	name: String,
// 	projectName: String,
// 	ipAddress:String,
// 	status: String,
// 	time: Date,
// }, { collection: 'collectionName' });

// var Project = mongoose.model('Project', projectSchemaName);

// var userStatusSchemaName = new Schema({
//     type: String,
// 	active: String

// }, { collection: 'collectionName' });

// var UserStatus = mongoose.model('UserStatus', userStatusSchemaName);

// var projectNameSchemaName = new Schema({
// 	name: String,
// 	platform: String,
// 	duration: String,

// }, { collection: 'collectionName' });

// var ProjectName = mongoose.model('ProjectName', projectNameSchemaName);


app.get('/test', function (req, res) {
	console.log("I got the request");
	Project.create({
		name: '15th Generation',
		ipAddress: '11111111',
		status: 'stop',
		time: new Date()
	}, (err, model) => {
		res.json(model);
	})

})

app.get('/user/status', function (req, res) {
	console.log("I got the user request");
	Status.create({
		name: 'Offline',
		is_active: true
	}, (err, model) => {
		res.json(model);
	})
	//res.json({status:"hello"});
	/*Model.find()
		.exec()
		.then(result => {
			res.json(result);
		})*/

})

app.get('/user/create', function (req, res) {
	console.log("I got the user request");
	User.create({
		name: 'Mishor',
		is_active: true
	}, (err, model) => {
		res.json(model);
	})
	//res.json({status:"hello"});
	/*Model.find()
		.exec()
		.then(result => {
			res.json(result);
		})*/

})



app.get('/project/create', function (req, res) {
	//console.log(req);
	console.log("I got the user request");
	Project.create({

		name: 'Rehab Housing',
		//name: req.body.projectname,
		is_active: true,
	}, (err, model) => {
		res.json(model);
	})

})


app.get('/projects', function (req, res) {
	//console.log(req);
	console.log("I got the user request");
	Project.find()
		.exec()
		.then(result => {
			res.json(result);
		})

})

app.get('/users', function (req, res) {
	//console.log(req);
	console.log("I got the user request");
	User.find()
		.exec()
		.then(result => {
			res.json(result);
		})

})

app.get('/timeinterval/screenshot', function (req, res) {
	//console.log(req);
	console.log("I got the user request");
	TimeInterval.find()
		.exec()
		.then(result => {
			res.json(result);
		})

})

app.get('/status', function (req, res) {
	//console.log(req);
	console.log("I got the user request");
	Status.find()
		.exec()
		.then(result => {
			res.json(result);
		})

})


app.get('/myProjects', function (req, res) {
	console.log("I got the user request");
	var projectName = mydb.ProjectName.find()
		.exec()
		.then(projectName => {
			res.json(projectName);
			console.log(projectName);
		})

})



app.get('/user', function (req, res) {
	//console.log(req);
	console.log("I got the user request");
	MonitorUser.find()
		.exec()
		.then(result => {
			res.json(result);
		})

})

app.put('/user-project-detail/:id', function (req, res) {
	//console.log(req);
	MonitorUser.findOne({ '_id': req.params.id })
              .populate('projects')
              .exec()
              .then(monitoruser=>{
                 res.json(monitoruser);
                console.log('The project is:', monitoruser.projects);
                  
              })

})


app.get('/test2/', function (req, res) {

	console.log(req.query, "I got the request1111111111111111111111111"); //_id: "58e47956f06cb610e49882cd",
	//Project.findOne({ ipAddress: req.query.ip, project_name: ''})
	Project.findOne({ ipAddress: req.query.ip })
		.exec()
		.then(project => {
			project.time = new Date();
			//result.name = 'kanaz'; // status: req.params.status
			project.status = req.query.status;
			project.save(err => {
				res.json(project);
			})

		})

})




app.get('/auth/loggedIn', function (req, res) {

	var str = req.headers.cookie;
	if (str) {
		var token = str.split("n=")[1];
		if (token) {
			// verifies secret and checks exp
			jwt.verify(token, config.SESSION_SECRET, function (err, decoded) {
				if (err) {
					return res.json({ success: false, message: 'Failed to authenticate token.' });
				} else {
					let user = decoded._doc;
					res.json({
						success: true,
						user: user.local.username
						///user: user.username
					});

				}
			});
		}
	} else {
		return res.json({
			success: false,
			message: 'No token provided.'
		});

	}

})

// app.get('/test2/?:status', function (req, res) {

// 	console.log(req.params, "I got the request1111111111111111111111111"); //_id: "58e47956f06cb610e49882cd",
// 	Project.findOne({ _id: "58e47956f06cb610e49882cd"})
// 		.exec()
// 		.then(project => {
// 			project.time = new Date();
// 			//result.name = 'mamun'; // status: req.params.status
// 			project.status = req.params.status;
// 			project.save(err => {
// 				res.json(project);
// 			})

// 		})

// })

app.use(myParser.urlencoded({ extended: true }));
app.use(myParser.json());
app.use(function (req, res, next) {
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();//not
});

app.use(cors());
const upload = multer({
	storage: multer.diskStorage({
		//destination: 'uploads/image/',
		destination: (req, file, callback) => {
			var currentdate = new Date();
			var date = currentdate.getDate() + "_"
				+ (currentdate.getMonth() + 1) + "_"
				+ currentdate.getFullYear();
			var newDateTimeString = String(date);
			let user = req.body.userName;
			let path = `./uploads//${user}/${newDateTimeString}`;
			fs.mkdirsSync(path);
			callback(null, path);
		},

		filename: (req, file, cb) => {

			let ext = path.extname(file.originalname);
			cb(null, `${Math.random().toString(36).substring(7)}${ext}`);
			//cb(null, `${req.file}${ext}`);
		}
	})
});

// app.post('/upload', (req, res) => {
// 	console.log(res.body);
// res.json(res.body);
// })

app.post('/upload', upload.any(), (req, res) => {
	console.log("me server side");


	var userId = req.body.userID;
	var username = req.body.userName;
	var pathString = req.files[0].path;
	var imageName = req.files[0].filename;
	var macAddress = req.body.mac;
	var ipAddress = req.body.ipAdd2;
	var imageResized = "resized";
	var thumb = "thumb";
   
	var rest = pathString.substring(0, pathString.lastIndexOf("/") + 1);

	var dirMypathString = path.join(rest + thumb + '/');
	// var dir = './uploads/'; 
	//     alert("before fs");
	if (!fs.existsSync(dirMypathString)) {
		fs.mkdirSync(dirMypathString)
	}

	else {

	  console.log("No path");
	}

	 var thumbImagePath = dirMypathString + imageName;
	fs.readFile(pathString, function (err, data) {
		fs.writeFile(dirMypathString, data, 'base64', function (err) {
			thumbImagePath = dirMypathString + imageName;
			im.resize({
				srcPath: pathString,
				dstPath: thumbImagePath,
				width: 300,
				height: 200,
			}, function (err, stdout, stderr) {
				if (err) throw err;
			});
		});

	});

	// fs.readFile(pathString, function (err, data) {
	// 	if (err) throw err;
	// 	fs.writeFile(pathString + imageName , data, 'base64', function (err) {
	// 		if (err) {
	// 			console.log("write file error", err);
	// 		}
	// 		else {
	// 			res.json({ success: true });
	// 			console.log("file uploaded");
	// 		}

	// 	});


	// });

	// console.log("body:", req.body);

	// console.log("file:", req.files);


	// var currentdate = new Date();
	// var date = currentdate.getDate() + "_"
	// 	+ (currentdate.getMonth() + 1) + "_"
	// 	+ currentdate.getFullYear();
	// var newDateTimeString = String(date);

	// var time = currentdate.getHours() + ":"
	//                     + currentdate.getMinutes() + ":"
	//                     + currentdate.getSeconds();
	// var newTime = String(time);					

	//var time = req.body.screenTime;
	var time;
	// console.log("path:", pathString);
	// console.log("imageName:", imageName);
	console.log("thumbURL",thumbImagePath);
	var imageFullName = path.join(macAddress + "-" + ipAddress);
	var screenshot = new Screenshot();
	screenshot.userId = userId;
	screenshot.username = username;
	screenshot.url = pathString;
	screenshot.imageName = imageName;
	screenshot.imageFullName = imageFullName;
	screenshot.thumbUrl= thumbImagePath;
	screenshot.macAdd = macAddress;
	screenshot.ipAdd = ipAddress;
	screenshot.timeqe = new Date();

	screenshot.save(function (err, result) {
		if (err) console.log("error occured");

	});
	res.json({ success: true });
	// if (result) {
	// 	console.log("Uploaded Successfully");
	// 	console.log(result);
	// }

	//res.json(result)





	// res.json(req.my_file.map(file => {
	//         let ext = path.extname(file.originalname);
	//         return {
	//             originalName: file.originalname,
	//             filename: file.filename
	//         }
	//     }));
	//  console.log("I will Upload");
	// 	res.json({status:"hello"});
})

/*app.post('/uploads', (req, res) => {

	//console.log(connect);
	var myString = req.body.name;
	var imageString = req.body.imageName;
	var macAddress = req.body.mac;
	var ipAddress = req.body.ipAdd;
	var time = req.body.screenTime;
	var tempPath = myString;
	//var filename = path.parse(image).base;
	console.log(tempPath);
	var pathElements = tempPath.replace(/\/$/, '').split('/');
	var lastFolder = pathElements[pathElements.length - 1];
	console.log("imagename:", lastFolder);
	var tarpath = 'E:/Electron/PackAndDist/electron-screemon/server/uploads/';

	fs.readFile(tempPath, function (err, data) {
		if (err) throw err;
		fs.writeFile(tarpath + imageString, data, 'base64', function (err) {
			if (err) {
				console.log("write file error", err);
			}
			else {
				res.json({ success: true });
				console.log("file uploaded");
			}

		});


	});
	var savedata = new Model({
		'myScreenString': tempPath,
		'macAdd': macAddress,
		'ipAdd': ipAddress,
		'time': time,
	}).save(function (err, result) {
		if (err) console.log("error occured");
		//var targetPath = path.resolve('./Serveruploads/', myString);
		var targetPath = path.resolve('E:/Electron/PackAndDist/electron-screemon/server/uploads/',imageString);
		console.log("Mac:" + macAddress);
		console.log("ipAdd:" + ipAddress);
		console.log("time:" + time);
		console.log("target path:" + targetPath)
		if (true) {
			fs.rename(tempPath, targetPath, function (err, result) {
				if (err) throw err;
				console.log("Upload completed!");
				res.json(req.body);
			});
		} else {
			fs.unlink(tempPath, function () {
				if (err) throw err;
				console.error("Only .png files are allowed!");
			});
		}

		if (result) {
			console.log(result);
			//res.json(result)
		}
	});

	//mongoose.connection.close();
})*/

// app.use(cors());
//     const upload = multer({
//         storage: multer.diskStorage({
//             destination: 'uploads/',
//             filename: (req, file, cb) => {
//                 let ext = path.extname(file.originalname);
//                 cb(null, `${Math.random().toString(36).substring(7)}${ext}`);
//             }
//         })
//     });

//    app.post('/upload', upload.any(), (req, res) => {
//         res.json(req.files.map(file => {
//             let ext = path.extname(file.originalname);
//             return {
//                 originalName: file.originalname,
//                 filename: file.filename
//             }
//         }));
//     });


app.post('/history/create', (req, res) => {
	console.log(req.headers.cookie);
	//var name = req.body.name;
	var status = req.body.status;
	var project = req.body.project;
	var ipAddress = req.body.ipAddress;
	var macAdd = req.body.macAdd;

	var str = req.headers.cookie;
	var token = str.split("n=")[1];
	if (token) {
		// verifies secret and checks exp
		jwt.verify(token, config.SESSION_SECRET, function (err, decoded) {
			if (err) {
				return res.json({ success: false, message: 'Authenticate failed, Login first' });
			} else {
				let user = decoded._doc;
				//res.json(user);
				console.log(user);
				var username = user.local.username;
				///var username = user.username;
				var history = new HistoryUser();
				history.userName = username;
				history.status = status;
				history.projectName = project;
				history.ipAddress = ipAddress;
				history.macAddress = macAdd;
				history.start_date = new Date();
				history.is_active = true;
				history.save(function (err, result) {
					if (err) console.log("error occured");

				});

				res.json({
					success: true,
					name: username,
					//user: { username: user.local.username },
					user: { username: user.username },
					message: 'Enjoy your token!',
					token: token
				});
				console.log("I got the user request");
				// var HistoryRecord = new HistoryUser({
				// 	'userName': name,
				// 	'status': status,
				// 	'projectName': project,
				// 	'ipAddress': ipAddress,
				// 	'macAddress': macAdd,
				// 	'start_date': new Date(),
				// 	'is_active': true,
				// }).save(function (err, result) {
				// 	if (err) console.log("error occured");

				// });
				// console.log("I got the user request");

			}
		});
	} else {
		return res.status(403).send({
			success: false,
			message: 'No token provided.'
		});

	}

	//   HistoryUser.create({
	// 	userName: name,
	// 	status: status,
	// 	projectName: project,
	// 	ipAddress: ipAddress,
	// 	macAddress: macAdd ,
	// 	start_date: new Date(),
	// 	is_active: true,
	// 	}, (err, model) => {
	// 	res.json(model);
	// })

})



app.post('/register/user', (req, res) => {

	console.log("/register/user");
	//console.log(req.body);
	var firstName = req.body.first_name;
	var lastName = req.body.last_name;
	var username = req.body.username;
	var email = req.body.email;
	var password = req.body.password;
	var localpassword = passwordHash.generate(password);


	///User.findOne({ "local.username": req.body.username })
	///MonitorUser.findOne({ "username": req.body.username })
	MonitorUser.findOne({ "local.username": req.body.username })
		.exec()
		.then(result => {
			if (result) {
				res.json({
					exist: true,
					message: 'User Already Exist!!',
				});
			}
			else {
				var user = {
					username: username,
					password: localpassword,
					local: { username: username, password: localpassword, email: email },
					first_name: firstName,
					last_name: lastName,
					is_active: true,
				};

				console.log(user);

				MonitorUser.create(user, (err, model) => {
					///User.create(user, (err, model) => {
					if (err) console.log(err);
					res.json({
						success: true,
						message: 'Successfully Registered!!'

					});
				})

			}



		})






	// if (findUser) {
	// 	res.json({
	// 		exist: true,
	// 		user: user,
	// 		message: 'User Already Exist!!',
	// 	});
	// }

	// else {
	// 	var user = {
	// 		local: { username: username, password: localpassword, email: email },
	// 		first_name: firstName,
	// 		last_name: lastName,
	// 		is_active: true,
	// 	};

	// 	console.log(user);

	// 	User.create(user, (err, model) => {
	// 		if (err) console.log(err);
	// 		//res.json(model);
	// 		res.json({
	// 			success: true,
	// 			message: 'Successfully Registered!!'

	// 		});
	// 		console.log(res);

	// 	})

	// }

	// User.findOne({ "local.username": req.body.username })
	// 	.exec()
	// 	.then(result => {
	// 		if (result.local.username) {
	// 			res.json({
	// 				exist: true,
	// 				user: user,
	// 				message: 'User Already Exist!!',
	// 			});
	// 			console.log(res);
	// 		}

	// 		else {


	// 	}
	// })



	console.log("I register  request");

})

// app.post('/timeinterval/screenshot', function (req, res) {
// 	console.log("I got the user request");
// 	TimeInterval.create({
// 		time_interval: req.body.time_interval

// 	}, (err, model) => {
// 		res.json(model);
// 	})
// 	//res.json({status:"hello"});
// 	/*Model.find()
// 		.exec()
// 		.then(result => {
// 			res.json(result);
// 		})*/

// })



// var user = {
// 	local: { username: username, password: localpassword, email: email },
// 	first_name: firstName,
// 	last_name: lastName,
// 	is_active: true,
// };

// console.log(user);

// User.create(user, (err, model) => {
// 	if (err) console.log(err);
// 	//res.json(model);
// 	res.json({
// 		success: true,

// 	});

// })




app.post('/logIn/user', (req, res) => {

	var name = req.body.username;
	var password = req.body.password;

	///User.findOne({ "local.username": req.body.username })
	///MonitorUser.findOne({ "username": req.body.username })
	MonitorUser.findOne({ "local.username": req.body.username })

		.exec()
		.then(user => {

			var dbPassword = user.local.password;
			///var dbPassword = user.password;
			let passwordverify = passwordHash.verify(password, dbPassword)

			if (passwordverify) {

				var token = jwt.sign(user, config.SESSION_SECRET, { expiresIn: 60 * 60 * 24 });
				res.cookie('token', token);
				res.json({
					success: true,
					//user: { username: user.local.username },
					user: user,
					message: 'Enjoy your token!',
					token: token,
					timeInterval: 10000,
				});
				console.log(user);
			} else {
				res.json({
					success: false,
					message: 'Authentication failed. Wrong password.'
				});
			}



		})
	// console.log(req);
	// var firstName = req.body.first_name;
	// var lastName = req.body.last_name;
	// var username = req.body.username;
	// var password = req.body.password;
	// var localpassword = passwordHash.generate(password);
	// var email= req.body.email;

	// User.create({
	// 	  local:{username: username,password:localpassword, email:email,},
	// 	  first_name: firstName,
	// 	  last_name: lastName, 
	// 	is_active: true,
	// 	}, (err, model) => {
	// 		 if (err) console.log(err);
	// 	res.json(model);

	// })

	// console.log("I register  request");
})


app.post('/me', (req, res) => {
	console.log(req);
	res.json('');

})



/*app.post('/upload', (req, res)=> {
 console.log("I will Upload");
	res.json({status:"hello"});
})*/

app.listen(PORT, function () {
	console.log('Example app listening on port ' + PORT + '!')
})