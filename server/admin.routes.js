
import authRoutes from './routes/admin/_authentication.router.js';

import config from '../config/config.json';

import roleRoutes from './routes/admin/_role.router.js';
import userRoutes from './routes/admin/_user.router.js';
import bonusRoutes from './routes/admin/_bonus-amount.router.js';
import paymentRoutes from './routes/admin/_payment.router.js';
import memberRoutes from './routes/admin/_member.router.js';
import areaRoutes from './routes/admin/_area.router.js';
import menuRoutes from './routes/admin/_menu.router.js';
//********************HR Routes
import departmentRoutes from './routes/admin/hr/hr.department.router.js';
import designationRoutes from './routes/admin/hr/hr.designation.router.js';
import employeeRoutes from './routes/admin/hr/hr.employee.router.js';

// *******************inventory
import categoryRoutes from './routes/admin/inventory/_category.router.js';
import suppierRoutes from './routes/admin/inventory/_supplier.router.js';
import productRoutes from './routes/admin/inventory/_product.router.js';
import storeRoutes from './routes/admin/inventory/_store.router.js';
import purchaseRoutes from './routes/admin/inventory/_purchase.router.js';
import issueRoutes from './routes/admin/inventory/_issue.router.js';
import transferRoutes from './routes/admin/inventory/_transfer.router.js';
import adjustmentRoutes from './routes/admin/inventory/_adjustment.router.js';
import stockRoutes from './routes/admin/inventory/_stock.router.js'

// *******************Account
import acccoaRoutes from './routes/admin/account/_acccoa.router.js';
import transactionRoutes from './routes/admin/account/_transaction.router';

// *******************voucher
import voucherRoutes from './routes/admin/_voucher.router.js';


import multer from 'multer';
import cors from 'cors';
import express from 'express';
import voucher_codes from 'voucher-code-generator';

import projectRoutes from './routes/admin/_project.router.js';
import monitorUserRoutes from './routes/admin/_monitor-user.router.js';


export default (app, router, passport) => {

    // ### Express Middlware to use for all requests
    router.use((req, res, next) => {

        // Make sure we go to the next routes and don't stop here...
        next();
    });


    function isLoggedIn(req, res, next) {
        // console.log(req.user.role);
        // if user is authenticated in the session, carry on
        if (req.isAuthenticated())
            return next();

        // if they aren't redirect them to the home page
        res.send({ staus: 420 });
    }

    function checkPermission(req) {

        // var requestedUrl = req.url,
        //     requestedMethod = req.method,
        //     role = req.user.role,
        //     hasUrl = (role.resources[requestedUrl].isAllowed) == undefined ? false : role.resources[requestedUrl].isAllowed,
        //     hasAction = (role.resources[requestedUrl].permissions[requestedMethod]) == undefined ? false : role.resources[requestedUrl].permissions[requestedMethod];
        if (hasUrl && hasAction)
            return true;

        return false;
    }

    let isPermitted = (req, res, next) => {
        let role_name = req.user.role.name.toLowerCase();
        if (req.isAuthenticated && role_name == "admin") {
            next();
        } else {
            if (!req.isAuthenticated() || !checkPermission(req)) {
                res.send(401);
            }
            else
                next();
        }
    };

    // Define a middleware function to be used for all secured routes
    let auth = (req, res, next) => {
        if (!req.isAuthenticated())
            res.json({ name: "Authentication Failed", message: "Please login first" });

        else
            next();
    };


    // #### RESTful API Routes

    // ### Server Routes

    // Handle things like API calls,

    // #### Authentication routes

    // Pass in our Express app and Router.
    // Also pass in auth & admin middleware and Passport instance
    authRoutes(app, router, passport, auth, isPermitted);

    roleRoutes(app, router, auth, isPermitted);
    userRoutes(app, router, auth, isPermitted);
    departmentRoutes(app, router, auth, isPermitted);
    designationRoutes(app, router, auth, isPermitted);
    employeeRoutes(app, router, auth, isPermitted);
    bonusRoutes(app, router, auth, isPermitted);
    paymentRoutes(app, router, auth, isPermitted)
    memberRoutes(app, router, passport, auth, isPermitted);
    areaRoutes(app, router, auth, isPermitted);
    menuRoutes(app, router, auth, isPermitted);

    projectRoutes(app, router, auth, isPermitted);
    monitorUserRoutes(app, router, auth, isPermitted);
    //**********************Inventory
    categoryRoutes(app, router, auth, isPermitted);
    suppierRoutes(app, router, auth, isPermitted);
    productRoutes(app, router, auth, isPermitted);
    storeRoutes(app, router, auth, isPermitted);
    purchaseRoutes(app, router, auth, isPermitted);
    issueRoutes(app, router, auth, isPermitted);
    transferRoutes(app, router, auth, isPermitted);
    adjustmentRoutes(app, router, auth, isPermitted);
    stockRoutes(app, router, auth, isPermitted);
    //**********************Account
    acccoaRoutes(app, router, auth, isPermitted);
    transactionRoutes(app, router, auth, isPermitted, voucher_codes);

    //**********************voucher
    voucherRoutes(app, router, auth, isPermitted);

    // File Upload
    const path = require('path');
    app.use(cors());
    // const upload = multer({
    //     storage: multer.diskStorage({
    //         destination: 'uploads/',
    //         filename: (req, file, cb) => {
    //             let ext = path.extname(file.originalname);
    //             cb(null, `${Math.random().toString(36).substring(7)}${ext}`);
    //         }
    //     })
    // });    

    // app.post('/upload', upload.any(), (req, res) => {
    //     res.json(req.files.map(file => {
    //         let ext = path.extname(file.originalname);
    //         return {
    //             originalName: file.originalname,
    //             filename: file.filename
    //         }
    //     }));
    // });

    var fs = require('fs'),
        url = require('url'),
        dir = config.IMAGE_UPLOAD_PATH;

    app.get('/images/:model/:name', (req, res) => {
        var request = url.parse(req.url, true);
        var action = request.pathname;
        var model = req.params.model + '/';
        if (action == ('/images/' + model + req.params.name)) {
            var img = fs.readFileSync(dir + model + req.params.name);
            res.writeHead(200, { 'Content-Type': 'image/gif' });
            res.end(img, 'binary');
        } else {
            var img = fs.readFileSync(dir + 'no-photo.png');
            res.writeHead(200, { 'Content-Type': 'image/gif' });
            res.end(img, 'binary');
        }
    })

    app.get('/img/:name', (req, res) => {
        var request = url.parse(req.url, true);
        var action = request.pathname;
        if (action == ('/img/' + req.params.name)) {
            var img = fs.readFileSync(dir + req.params.name);
            res.writeHead(200, { 'Content-Type': 'image/gif' });
            res.end(img, 'binary');
        } else {
            var img = fs.readFileSync(dir + 'no-photo.png');
            res.writeHead(200, { 'Content-Type': 'image/gif' });
            res.end(img, 'binary');
        }
    })

    // All of our routes will be prefixed with /admin/api
    app.use('/admin/api', router);
    app.use('', router);
};
