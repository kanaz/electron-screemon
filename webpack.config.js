var path = require('path');
var webpack = require('webpack');
var CommonsChunkPlugin = webpack.optimize.CommonsChunkPlugin;

module.exports = {
  devtool: 'source-map',
  debug: true,

  entry: {
    'angular2': [
      'rxjs',
      'reflect-metadata',
      'zone.js',
      '@angular/core',
      '@angular/router',
      '@angular/http'
    ],
	'polyfills': './src/polyfills.ts',
    'app': './main.ts'
    
  },

  output: {
    path: __dirname + '/dist/',
    publicPath: 'dist/',
    filename: '[name].js',
    sourceMapFilename: '[name].js.map',
    chunkFilename: '[id].chunk.js'
  },

  resolve: {
    extensions: ['','.ts','.js','.json', '.css', '.html']
  },

  module: {
    loaders: [
      {
        test: /\.ts$/,
        loader: 'ts',
        exclude: [ /node_modules/ ]
      }
    ]
  },

  plugins: [
    new CommonsChunkPlugin({ name: 'main', filename: 'main.js', minChunks: Infinity }),
    new CommonsChunkPlugin({ name: 'inline',   filename: 'inline.js' })
  ],
   target: 'electron-renderer'
};
