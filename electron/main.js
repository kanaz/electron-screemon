const electron = require('electron')
const main = require('electron-process').main;
var request = require('request')
const storage = require('electron-json-storage');
var path=require('path');
var AutoLaunch = require('auto-launch');
//const nativeImage =electron.nativeImage;
// Module to control application life.
const app = electron.app




// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.

let mainWindow

function createWindow () {
  // Create the browser window.
  //added me

  // storage.set('paths', [], (error)=>{
  //   console.log(error);
  // })
  
  const backgroundURL = 'file://' + __dirname + '/background.html';
  const backgroundProcessHandler = main.createBackgroundProcess(backgroundURL);

  mainWindow = new BrowserWindow({width:  360, 
                                  height: 640,
                                  icon:path.join(__dirname,'aro.ico'),
                                  });
  mainWindow.setMenu(null);
  // mainWindow.setTitle("PC ACTIVITY CHECK.COM");
  // mainWindow.getTitle();
  //added me
  backgroundProcessHandler.addWindow(mainWindow);
  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/index.html`)
 
  //added
  var minecraftAutoLauncher = new AutoLaunch({
    name: app.getName(),

  });

  minecraftAutoLauncher.enable();

  //minecraftAutoLauncher.disable();

  minecraftAutoLauncher.isEnabled()
  .then(function(isEnabled){
    if(isEnabled){
        return;
    }
    minecraftAutoLauncher.enable();
  })
  .catch(function(err){
      // handle error
  });

//added
  // Open the DevTools.
  console.log(app.getAppPath());

  
  mainWindow.webContents.openDevTools()
  
  //console.log(process.versions.electron );
 // console.log(app.getPath('userData'))
  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
    
  })
}



// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

 