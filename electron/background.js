'use strict'

const fs = require('fs');
const electron = require('electron');
const path = require('path');
var request = require('request');
var url = 'http://localhost:3000/test';
var os = require('os');
var ip = require('ip');
const remote = require('electron').remote;
const app = remote.app;


//declare var PouchDB:any;
//const storage = require('electron-json-storage');
const PouchDB = require('pouchdb');
var pathString;
var dateTimeString;
var newString;
var newDateTimeString;
var upload = "upload";
var myimage = "image";
var imageName;
var newImageName;
var mypathString;
var mynewString;
var currentdate = new Date();
var datetime = currentdate.getDate() + "."
    + (currentdate.getMonth() + 1) + "."
    + currentdate.getFullYear();
var time = currentdate.getHours() + ":"
    + currentdate.getMinutes() + ":"
    + currentdate.getSeconds();

//  PouchDB.plugin(require('pouchdb-adapter-leveldb'));
// var db = new PouchDB('todo');

const cpuIntensive = {

    doStuff() {
        var value = 0;
        for (var i = 0; i < 100000000; i++) {
            value += Math.random();
        }
        return Math.floor(value / Math.random());
    },


    myScreen() {
     

        //alert("beginning of myScreen");
        var realPath = app.getPath('documents');
        var applicationPath = app.getAppPath();
        electron.desktopCapturer.getSources({ types: ['window', 'screen'], thumbnailSize: { width: 100, height: 50 } }, (error, sources) => {
            if (error) throw error;
            for (let i = 0; i < sources.length; ++i) {
                console.log(sources[i]);

                navigator.webkitGetUserMedia({
                    audio: false,
                    video: {
                        mandatory: {
                            chromeMediaSource: 'desktop',
                            chromeMediaSourceId: sources[i].id,
                            minWidth: 1280,
                            maxWidth: 1280,
                            minHeight: 720,
                            maxHeight: 720
                        }
                    }
                }, gotStream, getUserMediaError);
                return;
            }

        });
        // alert("beginning of gotStream");
        function gotStream(stream) {
            // alert("inside of gotStream");
            var randomnumber = Math.floor(Math.random() * 100000);
            var video = document.createElement('video');
            video.addEventListener('loadedmetadata', function (e) {
                //alert("inside of video.addeventListener");
                var canvas = document.createElement('canvas');
                canvas.width = this.videoWidth;
                canvas.height = this.videoHeight;
                var ctx = canvas.getContext("2d");
                ctx.drawImage(this, 0, 0);
                var dot = ".";
                var url = canvas.toDataURL('image/jpeg', 1.0);
                var ext = url.split(';')[0].match(/jpeg|png|gif/)[0];
                var data1 = url.replace(/^data:image\/\w+;base64,/, "");
                var dirMypathString = path.join(realPath + '/' + upload + '/')
                // var dir = './uploads/'; 
                //     alert("before fs");
                if (!fs.existsSync(dirMypathString)) {
                    fs.mkdirSync(dirMypathString)
                }

                else {

                    //alert("canot make");
                }

                mypathString = path.join(realPath + '/' + upload + '/' + myimage);

                mynewString = String(mypathString);

                fs.writeFile(mynewString + dot + datetime + randomnumber + dot + ext, data1, 'base64', function (err) {

                    if (err) {
                        alert("An error ocurred creating the file " + err.message)
                    }


                    imageName = path.join(myimage + dot + datetime + randomnumber + dot + ext);
                    newImageName = String(imageName);
                    var currentdate = new Date();
                    var date = currentdate.getDate() + "."
                        + (currentdate.getMonth() + 1) + "."
                        + currentdate.getFullYear();
                    var time = currentdate.getHours() + ":"
                        + currentdate.getMinutes() + ":"
                        + currentdate.getSeconds();

                    dateTimeString = path.join(date + "-" + time);
                    newDateTimeString = String(dateTimeString);

                    pathString = path.join(mypathString + dot + datetime + randomnumber + dot + ext);
                    newString = String(pathString);

                    // pathString = path.join(mypathString + newDateTimeString + randomnumber + dot + ext);
                    // newString = String(pathString); new
                });


            }, false);

            video.src = URL.createObjectURL(stream);
            video.play();

        }


        function getUserMediaError(e) {
            console.log('getUserMediaError');
        }


        var interfaces = os.networkInterfaces();
        var addresses = [];
        var mac = [];
        var currentIp;
        var ipAdd2;
        for (var k in interfaces) {
            for (var k2 in interfaces[k]) {
                var address = interfaces[k][k2];
                if (address.family === 'IPv4' && !address.internal && address.address != '127.0.0.1' && address.address != '::1') {
                    
                    var str =address.address;
                    currentIp = str.startsWith("192");
                    if(currentIp)
                    {
                     addresses.push(address.address);
                     mac.push(address.mac);
                    }
                   
                }

            }
        }

        //  alert(JSON.stringify(addresses));
        //  var myip = interfaces.Ethernet[1].address;
        //  var mymac = interfaces.Ethernet[1].mac;

        ////var myip = ip.address('Ethernet');


        // var mac2 = mac[1];
        // var ipAdd2 = addresses[1];

        // var mac2 = mymac;
        // var ipAdd2 = myip;
        // alert(JSON.stringify(addresses));

        var mac2 = mac;
        var ipAdd2 = addresses;
        window.localStorage.setItem('applicationPath', applicationPath);
        var inter = interfaces;

        // if (mac2 === undefined) {
        //     mac2 = "No mac";

        // }

        // if (ipAdd2 === undefined) {
        //     ipAdd2 = "No ip";
        // }

        // window.localStorage.setItem('address', ipAdd2);

        //window.localStorage.setItem('mac', mac2);
        //comment r8

        // var infoUser = {nameString: newString, macAddress1: mac1, macAddress2: mac2, ipAddress1: ipAdd1, ipAddress2: ipAdd2}
        ///
        var infoUser = { nameString: newString, imageName: newImageName, macAddress2: mac2, ipAddress2: ipAdd2, screenTime: newDateTimeString}
        // var infoUser = { nameString: newString, imageName: newImageName, screenTime: newDateTimeString}
        // var infoUser = { nameString: newString, imageName: newImageName, screenTime: newDateTimeString, obj:ob }
        if (newString) {
            //alert("inside last if before return");
            return infoUser;
        }
        // if (newString !== undefined) {
        //     return infoUser;

        // }

    }


};


module.exports = cpuIntensive;