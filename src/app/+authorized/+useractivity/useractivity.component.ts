import { Component, OnInit } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { UserActivityService } from './useractivity.service';

declare var PouchDB: any;
var db = new PouchDB('mydb-idb');

export class Project {
  id: number;
  name: string;
}



@Component({
  selector: 'useractivity',
  templateUrl: './useractivity.component.html',
  providers: [UserActivityService]
})
export class UserActivityComponent implements OnInit {

  public projectName: any = [];
  public status: any = [];
  public history: any = {};
  // public users: any = [];
  public isAtWork: boolean;
  //public username: any;
  public username: any={};
  public user: any;
  public userId:any;
  public message: string;
  public userDetail:any = {};
  userProjects:any =[];

  constructor(private userActivityService: UserActivityService, private router: Router) {
    //this.loadUserStatus();
  }


  ngOnInit() {
    this.isAtWork = false;
    this.userActivityService.getAllStatus().subscribe(result => {
      this.status = result;
    })
    //this.username = window.localStorage.getItem('username');
    this.username = window.localStorage.getItem('userObject');
    var userObject =JSON.parse(this.username);
    
    this.user= userObject.local.username;
    this.userId = userObject._id;
    console.log("user Id:",this.userId);
    ///this.user= userObject.username;
    console.log(this.user);
    if (!this.username) {
      this.router.navigate(['/login']);
    }
  }


  projectSelected(status) {
    this.history.status = status;
    if (status == "At Work") {
      this.isAtWork = true;
      // this.userActivityService.getAllProject().subscribe(result => {
      //   this.projectName = result;
      // })
        this.userActivityService.getAllUserProject(this.userId).subscribe(result=>{
        this.userDetail = result;
        this.userDetail.email = result.local.email;
        this.userDetail.name = result.local.username;
        this.projectName =result.projects;
        console.log(this.userDetail);
        console.log( this.projectName);
        
        
        });


    }
    else {
      this.isAtWork = false;

    }
  }

  getStatus(project) {
    this.history.project = project;
  }

  saveHistory() {
    let address = window.localStorage.getItem('address');
    let macAdd = window.localStorage.getItem('mac');
    // console.log(this.history);
    this.history.ipAddress = address;
    this.history.macAdd = macAdd;
    //this.history.name= this.username ;
    this.userActivityService.postHistory(this.history).subscribe(result => {
      if (result.success) {
        this.message = "History saved successfully";
        setTimeout(() => {
          this.message = '';
        }, 2000);
      }
      else {
        this.message = result.message;
        setTimeout(() => {
          this.message = '';
        }, 2000);
      }
      console.log(JSON.stringify(result));
    })

    //console.log(this.history);
  }


  onclick(status) {


    let address = window.localStorage.getItem('address');

    let data = { status: status, ip: address }
    console.log(data);
    this.userActivityService.getLive(data).subscribe(result => {
      console.log(JSON.stringify(result));
    })

  }

  onclickAtWork() {
    this.userActivityService.getLiveProject().subscribe(result => {
      console.log(JSON.stringify(result));
    })
  }

  // createProject() {
  //   this.userActivityService.getCreateProject().subscribe(result => {
  //     console.log(JSON.stringify(result));
  //   })
  // }

  // createStatus() {
  //   this.userActivityService.getCreateStatus().subscribe(result => {
  //     console.log(JSON.stringify(result));
  //   })
  // }

  // createUser() {
  //   this.userActivityService.getCreateUser().subscribe(result => {
  //     console.log(JSON.stringify(result));
  //   })
  // }

  getAllProjectName() {
    this.userActivityService.getAllProject().subscribe(result => {
      console.log(JSON.stringify(result));
    })
  }

  logOut() {
    window.localStorage.removeItem('userObject');
    window.localStorage.removeItem('timeInterval');
    //window.localStorage.removeItem('username');
    this.router.navigate(['/login']);
  }



}
