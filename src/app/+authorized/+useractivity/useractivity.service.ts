import { Injector, Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { ConfigService } from '../../../services/config.service';

@Injectable()
export class UserActivityService {
    private apiURL: any;
    protected http: Http;
    protected headers;
    constructor(injector: Injector, private configService: ConfigService) {
        this.http = injector.get(Http);
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.apiURL = this.configService.apiURL();


    }

    // protected http: Http;
    // protected headers;
    // constructor(injector: Injector) {
    //     this.http = injector.get(Http);
    //     this.headers = new Headers();
    //     this.headers.append('Content-Type', 'application/json');


    // }



    // // get(name){


    // //     console.log("I am service");
    // //     return this.http.get('http://localhost:3000/test',this.headers).map(res=>res.json());
    // // }


    get() {
        return this.http.get(this.apiURL + '/userStatus', this.headers).map(res => res.json());
    }

    //  getName() {
    //     return this.http.post('http:/localhost:3000/Create', this.headers).map(res => res.json());
    // }


    getLive(body: any) {
        return this.http.get('http:/localhost:3000/test2?status=' + body.status + '&ip=' + body.ip, this.headers).map(res => res.json());
    }
    //  getLive(id: any) {
    //     return this.http.get('http:/localhost:3000/test2/'+ id+'&ip=1231321321321', this.headers).map(res => res.json());
    // }

    getLiveProject() {
        return this.http.get(this.apiURL + '/myProjects', this.headers).map(res => res.json());
    }

    getCreateProject() {
        return this.http.get(this.apiURL + '/project/create', this.headers).map(res => res.json());
    }
    getCreateStatus() {
        return this.http.get(this.apiURL + '/user/status', this.headers).map(res => res.json());
    }
    getCreateUser() {
        return this.http.get(this.apiURL + '/user/create', this.headers).map(res => res.json());
    }

    //  getCreateHistory()
    // {
    //     return this.http.get('http:/localhost:3000/history/create',this.headers).map(res => res.json());
    // }

    getAllProject() {
        return this.http.get(this.apiURL + '/projects', this.headers).map(res => res.json());
    }

    //     getAllUser()
    // {
    //     return this.http.get('http:/localhost:3000/users',this.headers).map(res => res.json());
    // }

    getAllStatus() {
        return this.http.get(this.apiURL + '/status', this.headers).map(res => res.json());
    }


    postHistory(data) {
        return this.http.post(this.apiURL + '/history/create', data, this.headers).map(res => res.json());
    }



    getAllUserProject(id)
    {
        return this.http.put(this.apiURL+ '/user-project-detail/' + id, { headers: this.headers }).map(res => res.json());
    }


}