import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { AuthorizedComponent } from './authorized.component';
import { RegisterComponent } from './+register/register.component';
import { RegisterService }  from './+register/register.service';
import { HomeComponent }  from './+home/home.component';
import {HomeService} from './+home/home.service';
// import {UserActivityComponent } from './+useractivity/useractivity.component';
// import {UserActivityService } from './+useractivity/useractivity.service';
//import {Page2Component } from './+page2/page2.component';
//import {Page2Service}  from './+page2/page2.service';

const routes: Routes = [
  {
    path: '',
    component: AuthorizedComponent,
    children: [
        {
        path: '',
        redirectTo: 'home',
      },
        {
        path: 'home',
        component: HomeComponent,
      },

          {
        path: '',
        redirectTo: 'register',
      },
   
      {
        path: 'register',
        component: RegisterComponent,
      },
      // {
      //   path: 'page2',
      //   component: Page2Component,
      // },
      {
        path: 'loop',
        loadChildren: './+authorized/authorized.module#AuthorizedModule',
      },
    ]
  },

];

@NgModule({
  imports: [
   
    CommonModule,
    FormsModule,
    RouterModule.forChild( routes ),
  ],
  //declarations: [ AuthorizedComponent, RegisterComponent], r8
  declarations: [ AuthorizedComponent, RegisterComponent,HomeComponent],
  //declarations: [ AuthorizedComponent, Page1Component, Page2Component ],
  //providers: [Page2Service,Page1Service],
  providers: [RegisterService,HomeService],
  bootstrap: [ AuthorizedComponent ],
})

export class AuthorizedModule {}