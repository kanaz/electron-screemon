import { Component, OnInit } from '@angular/core';
// import { BrowserModule } from '@angular/platform-browser';
import { RegisterService } from './register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  providers: [RegisterService]
})
export class RegisterComponent implements OnInit {

  public person: any = {};
  public message:string;
  
  constructor(private registerService: RegisterService, private router: Router) { }

  ngOnInit() {


  }


  registerUser() {

    console.log(this.person);
    if (this.person.first_name != '') {

      this.registerService.registerUser(this.person).subscribe(result => {
        //var res =JSON.stringify(result.message);
        if (result.exist) {
          console.log(result.exist);
          this.message = 'User Already Exist!!';
          setTimeout(() => {
            this.message = '';
          }, 2000);
        }
        else {
          
          this.message = 'Registered Successfully';
          setTimeout(() => {
            this.message = '';
          }, 2000);
        }


      })
      // this.message = "Registered Successfully";
      // setTimeout(() => {
      //   this.message = '';
      // }, 2000);

    }
    else {
      this.message = "Please enter every details";
      setTimeout(() => {
        this.message = '';
      }, 2000);

    }
  }

  backToLogin() {
    this.router.navigate(['/login']);

  }




}
