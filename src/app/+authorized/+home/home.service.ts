import { Injector, Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { ConfigService } from '../../../services/config.service';

@Injectable()
export class HomeService {
    private apiURL: any;
    protected http: Http;
    protected headers;
    constructor(injector: Injector, private configService: ConfigService) {
        this.http = injector.get(Http);
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.apiURL = this.configService.apiURL();


    }
    // return this.http.post('http:/localhost:3000/logIn/user', data, this.headers).map(res => res.json());


    isloggedIn() {
        return this.http.get(this.apiURL + '/auth/loggedIn', this.headers).map(res => res.json());
    }



}
