import { Component, OnInit } from '@angular/core';
import { desktopCapturer, remote, dialog, ipcRenderer } from 'electron';
import { HomeService} from './home.service';
import { Router } from '@angular/router';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    providers: [HomeService]
})
export class HomeComponent implements OnInit { 
  public applicationPath :string;
  constructor(private homeService: HomeService, private router: Router) {
   
  }

   ngOnInit(){

   console.log("I am in authorized componet");
    this.applicationPath = window.localStorage.getItem('applicationPath');
    
   
    let isLoggedIn = window.localStorage.getItem('userObject');
    console.log(isLoggedIn);
    if(isLoggedIn)
    {
      this.router.navigate(['/useractivity']);
    }
    else
    {
      this.checkLogIn();
    }
  

   }

   checkLogIn()
   {
     this.homeService.isloggedIn().subscribe(result=>{
     console.log( "result");
     console.log(result);
      if(result.success==true)
      {

         this.router.navigate(['/useractivity']);
      }
      else
      {
        setTimeout(() => {
          this.router.navigate(['/login']);
        }, 3000);
        // this.router.navigate(['/login']);
      }
    });

   }
}