import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
//import { readFile } from 'fs';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MdButtonModule } from '@angular2-material/button';
import { MdCardModule } from '@angular2-material/card';
import { MdSidenavModule } from '@angular2-material/sidenav';
import { MdIconModule } from '@angular2-material/icon';
import { MdIconRegistry } from '@angular2-material/icon';
import { MdListModule } from '@angular2-material/list';
import { MdToolbarModule } from '@angular2-material/toolbar';

import { Ng2UploaderModule } from 'ng2-uploader';

// const electron = require('electron');



@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    AppRoutingModule,
    MdCardModule,
    MdButtonModule,
    MdSidenavModule,
    MdIconModule,
    MdSidenavModule.forRoot(),
    MdToolbarModule,
    MdListModule,
    Ng2UploaderModule,

    BrowserModule,

    //takeScreenshot,
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent],
  providers: [MdIconRegistry],
})
export class AppModule { }
