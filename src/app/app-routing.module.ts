import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { LoggedInGuard } from '../services/logged-in.guard';
import { UserService } from '../services/user';
import { UserActivityComponent } from './+authorized/+useractivity/useractivity.component';
import { ConfigService } from '../services/config.service';
// import { CookieService } from 'angular2-cookie/services/cookies.service';

import { LoginComponent } from './+login/login.component';


const routes: Routes = [

  {
    path: '',
    redirectTo: 'authorized',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'useractivity',
    component: UserActivityComponent,
  },


  {
    path: 'authorized',
    loadChildren: './+authorized/authorized.module#AuthorizedModule',
    canActivate: [LoggedInGuard],
  },
];


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes, { useHash: true })
  ],
  providers: [LoggedInGuard, UserService, ConfigService],
  declarations: [LoginComponent, UserActivityComponent],
  exports: [
    RouterModule
  ],
})

export class AppRoutingModule {


}
