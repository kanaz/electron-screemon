import { Injector, Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';
import { ConfigService } from '../../services/config.service';

@Injectable()
export class LoginService {
    private apiURL: any;
    protected http: Http;
    protected headers;
    constructor(injector: Injector, private configService: ConfigService) {
        this.http = injector.get(Http);
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
        this.apiURL = this.configService.apiURL();


    }


    loginUser(data) {
        return this.http.post(this.apiURL + '/logIn/user', data, this.headers).map(res => res.json());
    }

    // getTimeInterval()
    // {
    //     return this.http.get(this.apiURL + '/timeinterval/screenshot', this.headers).map(res => res.json()); 
    // }



}

