import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { UserService } from '../../services/user';
import { LoginService } from './login.service';
// import {CookieService} from 'angular2-cookie/core';

declare var PouchDB: any;
var db = new PouchDB('mydb-idb');
@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  providers: [LoginService]
})

export class LoginComponent {
  public person: any = {};
  private isLogged = false;
  public message: string;
  public applicationPath :string;
  // public timeInterval: number;

  constructor(private userService: UserService, private router: Router, private loginService: LoginService) {
    this.isLogged = userService.isLoggedIn();
  }

  ngOnInit(){
    this.applicationPath = window.localStorage.getItem('applicationPath');
    let isLoggedIn=window.localStorage.getItem('username');

    console.log(isLoggedIn);
    if(isLoggedIn)
    {
      this.router.navigate(['/useractivity']);
    }
  }

  submitLogin() {
    this.userService.login().subscribe(() => {
      this.router.navigate(['']);
    });
  }
  submitLogout() {
    this.userService.logout();
    this.isLogged = false;
  }



  getLoggedInUser(){
    this.loginService.loginUser(this.person).subscribe(result => {
      if (result.success == true){
        
        let timeInterval = result.timeInterval;
        let ipAddress = window.localStorage.getItem('address');
        let macAdd = window.localStorage.getItem('mac');
       
        //let user = result.user.username;
        let userObject = JSON.stringify(result.user);
       
        //window.localStorage.setItem('username',user);

        window.localStorage.setItem('timeInterval',timeInterval);
        window.localStorage.setItem('userObject',userObject);
        let token = result.token;
        // this._cookieService.put('token',token);
       
       /* r8
        console.log(result);
        console.log(user);
        var userInfo = {
          userName: user,
          userIpAddress: ipAddress,
          userMacAddress: macAdd,
          userToken: token,
        }

        db.post(userInfo, function (err, result) {
          console.log("userinfo:",result.id);
          db.allDocs({ include_docs: true, ascending: true }, function (err, result) {
           // console.log(result);

          });
    }); r8 */

        //for deleting
        /*  db.allDocs({ include_docs: true, ascending: true }, function (err, result) {
           console.log(result);
          for(var i=0;i<result.rows.length;i++){
    
           db.remove(result.rows[i].doc._id,result.rows[i].doc._rev);
    
          }
        });*/

        console.log(JSON.stringify(this.person));
        this.router.navigate(['/useractivity']);
      }
      else {
        this.message = 'Wrong Username or Password!! Try Again.';
        //  setTimeout(() => {
        //   this.message = '';
        // }, 2000);
        this.router.navigate(['/login']);

      }
    });

    //console.log(JSON.stringify(this.person));
  }

  getRegisterUser() {

    this.router.navigate(['../authorized/register']);
  }

}
