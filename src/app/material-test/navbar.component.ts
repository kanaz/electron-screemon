import {Component, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'navbar',
    templateUrl: 'navbar.component.html'
})
export class NavbarComponent {

    @Output() onSidenav = new EventEmitter<boolean>();

    onToggleSidenav() {

        this.onSidenav.emit();
    }
}