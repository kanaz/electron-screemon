import { Injector, Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';


@Injectable()
export class AppService {

    protected http: Http;
    protected headers;
    constructor(injector: Injector) {
        this.http = injector.get(Http);
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');


    }



    // get(name){


    //     console.log("I am service");
    //     return this.http.get('http://localhost:3000/test',this.headers).map(res=>res.json());
    // }


    get(data) {
        return this.http.post('http:/localhost:3000/upload', data, this.headers).map(res => res.json());
    }

    isloggedIn()
    {
        return this.http.get('http:/localhost:3000/auth/loggedIn', this.headers).map(res => res.json()); 
    }

}