import { Injector, Injectable } from '@angular/core';
import { Http, Headers, URLSearchParams } from '@angular/http';


@Injectable()
export class ConfigService {

    public baseUrl = "http:/192.168.101.67";
    protected http: Http;
    protected headers;
    constructor(injector: Injector) {
        this.http = injector.get(Http);
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
    }
    apiURL() {
        return this.baseUrl;
    }




}